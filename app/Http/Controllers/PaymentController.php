<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\Wishlist;
use App\Models\Help;
use App\Models\Payment;
use App\Models\Voucher;
use App\Models\Cart;
use App\Models\Rating;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    public function addPayment(Request $req, $id)
    {
        $checkPayment = Payment::find($id);
        $checkPayment->payment_method = $req->method;
        $checkPayment->status = 'Dikemas';
        $checkPayment->save();
        
        $allCart = Cart::where('user_id', Auth::user()->id)->get();
        foreach($allCart as $ac) {
            $ac->status = true;
            $ac->save();
        }
        return redirect('/imperfect/success');
    }
    
    public function indexOrder()
    {
        $allOrder = [];
        $order = Payment::where('user_id', Auth::user()->id)->whereNot('status', 'Keranjang')->whereNot('status', 'Selesai')->get();
        foreach($order as $o) {
            $perOrder = Cart::where('payment_id', $o->id)->get();
            if(count($perOrder) > 0){
                foreach($perOrder as $po){
                    array_push($allOrder, $po);
                }
            } else {
                array_push($allOrder, $perOrder);
            }
        }

        return view('order', ['title' => 'order', 'allOrder' => $allOrder]);
    }

    public function indexHistory()
    {
        $allOrder = [];
        $order = Payment::where('user_id', Auth::user()->id)->where('status', 'Selesai')->get();
        foreach($order as $o) {
            $perOrder = Cart::where('payment_id', $o->id)->get();
            if(count($perOrder) > 0){
                foreach($perOrder as $po){
                    array_push($allOrder, $po);
                }
            }
        }
        return view('history', ['title' => 'history', 'allOrder' => $allOrder]);
    }
}
