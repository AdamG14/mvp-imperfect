<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\Wishlist;
use App\Models\Help;
use App\Models\Payment;
use App\Models\Voucher;
use App\Models\Cart;
use App\Models\Rating;
use Illuminate\Support\Facades\Auth;

class ProdukController extends Controller
{
    public function indexHomepage()
    {
        $produk = Produk::paginate(12);
        $title  = 'homepage';
        return view('home')->with(compact('produk', 'title'));
    }

    public function indexHomepageByName(Request $req)
    {
        $produk = Produk::where('nama', 'LIKE', "%$req->nama%")->get();
        $title  = 'homepage';
        return view('home')->with(compact('produk', 'title'));
    }

    public function indexProduct($id)
    {
        $produk = Produk::find($id);
        $recom = Produk::whereNot('id', $id)->paginate(3);
        $review = Rating::where('produks_id', $id)->paginate(3);
        $averageReview = null;
        if(!empty($review[0])) {
            $total = 0;
            foreach($review as $r) {
                $total += $r->nilai;
            }
            $averageReview = round($total / count($review));
        }
        return view('product', ['title' => 'produk', 'produk' => $produk,'review' => $review, 'averageReview' => $averageReview, 'recom' => $recom]);
    }

    public function indexWishlist()
    {
        $allWish = Wishlist::where('user_id', Auth::user()->id)->get();

        return view('wishlist', [
            'title' => 'wishlist',
            'allWish' => $allWish
        ]);
    }

    public function addWishlist($id)
    {
        $checkWish = Wishlist::where('produks_id', $id)->first();
        if(empty($checkWish)) {
            $newWish = new Wishlist();
            $newWish->produks_id    = $id;
            $newWish->user_id       = Auth::user()->id;
            $newWish->save();
        } else {
            $checkWish->delete();
        }

        return redirect('/imperfect/home');
    }

    public function helpSubmit(Request $req)
    {
        $help = new Help();
        $help->judul = $req->judul;
        $help->isi = $req->isi;
        $help->user_id = Auth::user()->id;
        $help->save();

        return redirect('/imperfect/help');
    }
}
