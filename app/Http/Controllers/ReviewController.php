<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\Wishlist;
use App\Models\Help;
use App\Models\Payment;
use App\Models\Voucher;
use App\Models\Cart;
use App\Models\Rating;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    
    public function indexReview($id, $idCart){
        $produk = Produk::find($id);
        return view('review', ['title' => 'review', 'produk' => $produk, 'idCart' => $idCart]);
    }

    public function storeReview(Request $req, $id, $idCart){
        $produk = Produk::find($id);
        $newReview = new Rating;
        $newReview->produks_id = $id;
        $newReview->user_id = Auth::user()->id;
        $newReview->nilai = $req->rate;
        $newReview->ulasan = $req->deskripsi;
        if($req->file('ulasan_foto') != null) {
            $image = $req->file('ulasan_foto');
            $fileName = $image->getClientOriginalName();
            $image->move(public_path('img/ulasan/'), $fileName);
            $newReview->foto = $fileName;
        } else {
            $newReview->foto = '';
        }
        $newReview->save();
        $cart = Cart::find($idCart);
        $cart->rating_id = $newReview->id;
        $cart->save();
        
        return redirect('/imperfect/product/'.$id);
    }
}
