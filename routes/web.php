<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\PaymentController;

Route::get('/', function () {
    return view('index');
});

Route::get('/about', function () {
    return view('about', [
        "title" => "about"
    ]);
});

Route::get('/faq', function () {
    return view('faq', [
        'title' => 'faq'
    ]);
});

Route::get('/login', function () {
    return view('login', [
        'title' => 'login'
    ]);
})->name('login');;

Route::post('/login', [AuthController::class, 'login'])->name('loginCheck');
Route::get('/logout', [AuthController::class, 'logout'])->name('loginCheck');
Route::post('/sign-up', [AuthController::class, 'register'])->name('register');

Route::get('/sign-up', function () {
    return view('sign-up', [
        'title' => 'sign-up'
    ]);
});

Route::prefix('imperfect')->middleware('auth')->group(function () { 
    Route::get('/home', [ProdukController::class, 'indexHomepage']);
    Route::get('/home/search', [ProdukController::class, 'indexHomepageByName']);
    Route::get('/cart', [CartController::class, 'indexCart']);
    Route::get('/wishlist', [ProdukController::class, 'indexWishlist']);
    Route::get('/add-wishlist/{id}', [ProdukController::class, 'addWishlist']);
    Route::get('/add-cart/{id}', [CartController::class, 'addCart']);
    Route::post('/add-cart-with-value/{id}', [CartController::class, 'addCartValue']);
    Route::get('/add-cart-wish/{idWish}/{idProduk}', [CartController::class, 'addCartWish']);
    Route::post('/add-voucher/{id}', [CartController::class, 'addVoucher']);
    Route::post('/add-payment/{id}', [PaymentController::class, 'addPayment']);
    Route::get('/qty-min/{id}', [CartController::class, 'decreaseQuantity']);
    Route::get('/qty-plus/{id}', [CartController::class, 'increaseQuantity']);
    Route::get('/remove-cart/{id}', [CartController::class, 'removeCart']);
    Route::get('/review/{id}/{idCart}', [ReviewController::class, 'indexReview']);
    Route::post('/review/{id}/{idCart}', [ReviewController::class, 'storeReview']);
    Route::get('/order', [PaymentController::class, 'indexOrder']);
    Route::get('/history', [PaymentController::class, 'indexHistory']);
    Route::post('/change-profile', [AuthController::class, 'editProfile']);
    Route::get('/product/{id}', [ProdukController::class, 'indexProduct']);
    Route::get('/my-store', function () {
        return view('my-store', [
            'title' => 'my-store'
        ]);
    });

    Route::get('/success', function () {
        return view('transaction-success', [
            'title' => 'success'
        ]);
    });

    Route::get('/help', function () {
        return view('help', [
            'title' => 'help'
        ]);
    });
    Route::post('/help/submit', [ProdukController::class, 'helpSubmit']);
    
    Route::get('/profile', function () {
        return view('profile', [
            'title' => 'profile'
        ]);
    });
    
});
